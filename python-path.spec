%if 0%{?fedora} || 0%{?rhel} >= 7
%global with_python3 1
%endif

%global bname path

Name:           python-%{bname}
Version:        5.2
Release:        1.2%{?dist}
Summary:        A python module wrapper for os.path

License:        MIT
URL:            https://pypi.python.org/pypi/path.py
Source0:        https://pypi.python.org/packages/source/p/path.py/%{bname}.py-%{version}.zip

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-pytest

%description
path.py implements a path objects as first-class entities, 
allowing common operations on files to be invoked on those path objects directly.

See documentation here http://amoffat.github.io/sh/.

%package    -n python%{python3_pkgversion}-%{bname}
Summary:    Python 3 module wrapper for os.path
Group:      Development/Libraries

%description -n python%{python3_pkgversion}-%{bname}
path.py implements a path objects as first-class entities,
allowing common operations on files to be invoked on those path objects directly.

See documentation here http://amoffat.github.io/sh/.


%prep
%setup -q -n %{bname}.py-%{version}

%build
%{__python3} setup.py build 

#%check
#%{__python3} test_path.py

%install
%{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT


%files -n python%{python3_pkgversion}-path
%{python3_sitelib}/*


%changelog
* Thu Oct 24 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 5.2-1.2
- Drop support for python 3.4, python2

* Fri Nov 30 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 5.2-1.1
- Update to support python 3.6

* Wed Sep  3 2014 Thomas Spura <tomspur@fedoraproject.org> - 5.2-1
- update to 5.2

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed May 28 2014 Kalev Lember <kalevlember@gmail.com> - 5.1-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Fri Apr 04 2014 Xavier Lamien <laxathom@fedoraproject.org> - 5.1-1
- Upstream release.
- Add python3's subpackage.

* Fri Jul 26 2013 Xavier Lamien <laxathom@fedoraproject.org> - 4.3-1
- Upstream release.

* Wed Apr 10 2013 Xavier Lamien <laxathom@fedoraproject.org> - 3.0.1-2
- Add %%check stage.
- Update BuildRequire.
- Add missing %%docs.

* Wed Apr 10 2013 Xavier Lamien <laxathom@fedoraproject.org> - 3.0.1-1
- Initial RPM release.
